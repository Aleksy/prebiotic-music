# Archean Music Model

The AMM is a simple library which contains model objects of notes and music.
It can be download [here](https://drive.google.com/open?id=1xhkzsHYeaUED3vBo0IlCSDkZMjflCs_x).

## Work with AMM library

You have to include the JAR file into your project as external library.
By the AMM you can create and display simple music. The library objects definitions are
prepared in single-initialization design pattern, so interfaces has no setters.
To create a single note you have to make a decision about the note pitch, start point
and duration. For example:
```java
PitchEnum pitch = PitchEnum.B4;
int startPoint = 0;
int duration = 4;
```
Now you can create the note:
```java
Note note = new StandardNote(pitch, startPoint, duration);
```
If you have all needed notes, you can add it to the List and inject to the
new instance of Music interface:
```java
      int measuresPerBar = 4;
      int cellsPerMeasure = 3;
      Music music = new StandardMusic(notes, measuresPerBar, cellsPerMeasure);
```
And done. You can also use the builder:
```java
      int measuresPerBar = 4;
      int cellsPerMeasure = 4;
      Music music = new MusicBuilder()
         .addNote(PitchEnum.D3, 0, 2)
         .addNote(PitchEnum.E3, 2, 2)
         .addNote(PitchEnum.Fh3, 4, 2)
         .addNote(PitchEnum.D3, 6, 2)
         .addNote(PitchEnum.E3, 8, 4)
         .addNote(PitchEnum.D3, 12, 4)
         .create(measuresPerBar, cellsPerMeasure);
```
The music is displayable. You can see the music by creating the MusicPanel class
and injecting it to the JFrame:
```java
      double hue = 0.57;
      MusicPanel musicPanel = new MusicPanel(music, hue, NoteNameDisplayMode.AMERICAN, 20);
      
      JFrame frame = new JFrame("Frame");
      frame.setContentPane(musicPanel);
      frame.addMouseListener(musicPanel);
      frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
      frame.setVisible(true);
      frame.setSize(1000, 300);
      frame.setFocusable(true);
```
And that's the result:
![](img/musicpanel.png)

Music Panel is also the mouse listener. If you click and drag the mouse into the frame,
the music will move.