package aleksy.prebioticmusic.archean.musicmodel.display;

import aleksy.prebioticmusic.archean.musicmodel.enumerate.NoteNameDisplayMode;
import aleksy.prebioticmusic.archean.musicmodel.enumerate.PitchEnum;
import aleksy.prebioticmusic.archean.musicmodel.logic.NoteLogic;
import aleksy.prebioticmusic.archean.musicmodel.model.Music;
import aleksy.prebioticmusic.archean.musicmodel.model.Note;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Music panel to display the music matrix
 */
public class MusicPanel extends JPanel implements MouseListener {
   private int timeSteps;
   private Note lowest;
   private Music music;
   private int measuresPerBar;
   private int cellsPerMeasure;
   private int ambitus;
   private double hue;
   private NoteNameDisplayMode displayMode;
   private Color standardColor;
   private int mouseX;
   private int mouseY;
   private int prevMouseX;
   private int relativeMovedX;
   private boolean drawMouseRectangles;
   private boolean movingAvailable;

   /**
    * Constructor
    * @param music to display
    * @param hue of notes
    * @param displayMode decides if each note should have american or european name
    * @param timeSteps to display in the matrix
    */
   public MusicPanel(Music music, double hue, NoteNameDisplayMode displayMode, int timeSteps) {
      this.music = music;
      this.measuresPerBar = music.getMeasuresPerBar();
      this.cellsPerMeasure = music.getCellsPerMeasure();
      this.ambitus = music.calculateAmbitus();
      this.displayMode = displayMode;
      this.hue = hue;
      this.lowest = music.getLowestNote();
      this.relativeMovedX = 0;
      this.timeSteps = timeSteps;
      mouseX = 0;
      mouseY = 0;
      drawMouseRectangles = false;
      standardColor = new Color(6, 7, 17);
      setBackground(standardColor);
      addMouseListener(this);
   }

   @Override
   protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.setFont(new Font("Courier New", Font.PLAIN, 13));
      int width = getWidth();
      int height = getHeight();
      int measureOffset = 40;
      height -= measureOffset;
      int pitchSteps = ambitus + 2;
      double tableOffset = 50.0;
      double xStep = (width - tableOffset) / (double) timeSteps;
      double yStep = (height) / (double) pitchSteps;
      g.setColor(standardColor.brighter().brighter().brighter());
      g.fillRect(0, 0, (int) tableOffset, height);
      g.fillRect(0, height, width, getHeight());
      if (drawMouseRectangles) {
         g.setColor(new Color(32, 32, 48));
         g.fillRect((int) (((mouseX - (int) tableOffset) / (int) xStep) * xStep + tableOffset),
            0, (int) xStep, height);
         g.fillRect(0, (int) (mouseY / (int) yStep * yStep), width, (int) yStep);
         g.setColor(g.getColor().brighter().brighter());
         g.fillRect((int) (((mouseX - (int) tableOffset) / (int) xStep) * xStep + tableOffset),
            (int) (mouseY / (int) yStep * yStep), (int) xStep, (int) yStep);
         g.setColor(standardColor.brighter().brighter().brighter().brighter());
         g.fillRect(0, (int) (mouseY / (int) yStep * yStep), (int) tableOffset, (int) yStep);
      }
      for (int x = 0; x < timeSteps; x++) {
         g.setColor(standardColor.brighter().brighter().brighter().brighter());
         if ((x + 1 + relativeMovedX) % cellsPerMeasure == 0)
            g.setColor(g.getColor().brighter().brighter());
         if ((x + 1 + relativeMovedX) % (measuresPerBar * cellsPerMeasure) == 0) {
            g.setColor(g.getColor().brighter().brighter());
            g.drawLine((int) ((x + 1) * xStep + tableOffset) + 1,
               0, (int) ((x + 1) * xStep + tableOffset) + 1, height);
         }
         g.drawLine((int) ((x + 1) * xStep + tableOffset),
            0, (int) ((x + 1) * xStep + tableOffset), height);
         g.setColor(Color.GRAY);
         g.drawString(Integer.toString(x + relativeMovedX), (int) (x * xStep + tableOffset),
            height + measureOffset / 2);
      }
      for (int y = 0; y < pitchSteps; y++) {
         g.setColor(standardColor.brighter().brighter().brighter());
         g.drawLine((int) tableOffset, (int) ((y + 1) * yStep), width, (int) ((y + 1) * yStep));
         int pitch = y + lowest.getPitch().getPitchInteger();
         String name = null;
         if (displayMode == NoteNameDisplayMode.AMERICAN) {
            PitchEnum pitchEnum = NoteLogic.findPitchByNumber(pitch);
            if (pitchEnum != null)
               name = pitchEnum.getPitchAmericanString();
         }
         if (displayMode == NoteNameDisplayMode.EUROPEAN) {
            PitchEnum pitchEnum = NoteLogic.findPitchByNumber(pitch);
            if (pitchEnum != null)
               name = pitchEnum.getPitchEuropeanString();
         }
         if (name != null) {
            g.setColor(Color.GRAY);
            g.drawString(name, (int) (tableOffset * 0.3), (int) (height - (y + 1) * yStep + yStep / 1.6));
         }
      }
      for (Note note : music.getNotes()) {
         double x1, y1, x2, y2;
         x1 = (note.getStartPoint() - relativeMovedX) * xStep;
         x2 = (note.getDuration() + note.getStartPoint() - relativeMovedX) * xStep;
         y1 = height - (note.getPitch().getPitchInteger()
            - lowest.getPitch().getPitchInteger() + 1) * yStep;
         y2 = height - (note.getPitch().getPitchInteger()
            - lowest.getPitch().getPitchInteger()) * yStep;
         g.setColor(new Color(Color.HSBtoRGB((float) hue, (float) 0.67, (float) 0.64)));
         g.fillRect((int) (x1 + tableOffset), (int) y1, (int) (x2 - x1), (int) (y2 - y1));
         g.setColor(g.getColor().brighter().brighter());
         g.drawRect((int) (x1 + tableOffset), (int) y1, (int) (x2 - x1), (int) (y2 - y1));
      }
      if (movingAvailable) {
         if(prevMouseX < mouseX) {
            relativeMovedX -= (mouseX - prevMouseX) / (xStep * 0.3);
         }
         if(prevMouseX > mouseX) {
            relativeMovedX += (prevMouseX - mouseX) / (xStep * 0.3);
         }
         if(relativeMovedX < 0) relativeMovedX = 0;
      }
      prevMouseX = mouseX;
   }

   @Override
   public void mouseClicked(MouseEvent e) {
   }

   @Override
   public void mousePressed(MouseEvent e) {
      movingAvailable = true;
   }

   @Override
   public void mouseReleased(MouseEvent e) {
      movingAvailable = false;
   }

   @Override
   public void mouseEntered(MouseEvent e) {
      drawMouseRectangles = true;
      Point mousePosition = e.getLocationOnScreen();
      mouseX = mousePosition.x;
      mouseY = mousePosition.y;
      Thread animation = new Thread(() -> {
         while (drawMouseRectangles) {
            repaint();
            try {
               Point position = getMousePosition();
               mouseX = position.x;
               mouseY = position.y;
               Thread.sleep(20);
            } catch (Exception ignored) {
            }
         }
      });
      animation.start();
   }

   @Override
   public void mouseExited(MouseEvent e) {
      drawMouseRectangles = false;
      repaint();
   }
}
