package aleksy.prebioticmusic.archean.musicmodel.constants;

/**
 * Basic constants
 */
public class BasicConstants {
   /**
    * Name
    */
   public static final String NAME = "Archean Music Model";
   /**
    * Version
    */
   public static final String VERSION = "1.0.2";
   /**
    * Author
    */
   public static final String AUTHOR = "Aleksy Bernat, Wroclaw";
   /**
    * Short name
    */
   public static final String SHORT_NAME = "archmodel";
}
