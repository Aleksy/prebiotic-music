package aleksy.prebioticmusic.archean.musicmodel.logic;

import aleksy.prebioticmusic.archean.musicmodel.enumerate.Interval;
import aleksy.prebioticmusic.archean.musicmodel.enumerate.Key;
import aleksy.prebioticmusic.archean.musicmodel.enumerate.PitchEnum;
import aleksy.prebioticmusic.archean.musicmodel.model.Music;
import aleksy.prebioticmusic.archean.musicmodel.model.Note;
import aleksy.prebioticmusic.archean.musicmodel.model.impl.StandardMusic;
import aleksy.prebioticmusic.archean.musicmodel.model.impl.StandardNote;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Music logic class
 */
public class MusicLogic {
   /**
    * Combines two music matrices to one
    *
    * @param music1 to combine
    * @param music2 to combine
    * @return new music instance combined from parameters
    */
   public static Music combineToOne(Music music1, Music music2) {
      List<Note> notes = new ArrayList<>(music1.getNotes());
      notes.addAll(music2.getNotes());
      return new StandardMusic(notes, music1.getMeasuresPerBar(), music1.getCellsPerMeasure());
   }

   /**
    * Transposes all notes of the music matrix by interval
    *
    * @param music to transpose
    * @param interval of transposition
    * @param transposeDown if new music should be lower than previous
    * @return transposed music
    */
   public static Music transpose(Music music, Interval interval, boolean transposeDown) {
      List<Note> notes = new ArrayList<>();
      for (Note note : music.getNotes()) {
         int newPitch = note.getPitch().getPitchInteger() + interval.getInterval();
         if (transposeDown)
            newPitch = note.getPitch().getPitchInteger() - interval.getInterval();
         notes.add(new StandardNote(
            NoteLogic.findPitchByNumber(newPitch), note.getStartPoint(), note.getDuration()));
      }
      return new StandardMusic(notes, music.getMeasuresPerBar(), music.getCellsPerMeasure());
   }

   /**
    * Finds note pitches without octave doubling
    * @param music to research
    * @return list of {@link Key}
    */
   public static List<Key> getUsedNotes(Music music) {
      List<Note> notes = music.getNotes();
      Set<Key> keys = new HashSet<>();
      for(Note note : notes) {
         keys.add(note.getPitch().getKey());
      }
      return new ArrayList<>(keys);
   }
}
