package aleksy.prebioticmusic.archean.musicmodel.logic;

import aleksy.prebioticmusic.archean.musicmodel.enumerate.Interval;
import aleksy.prebioticmusic.archean.musicmodel.enumerate.PitchEnum;
import aleksy.prebioticmusic.archean.musicmodel.model.Note;

/**
 * Note logic class
 */
public class NoteLogic {
   /**
    * Calculates the interval between two notes
    * @param n1 first note
    * @param n2 second note
    * @return interval
    */
   public static int calculateInterval(Note n1, Note n2) {
      return Math.abs(n1.getPitch().getPitchInteger()
         - n2.getPitch().getPitchInteger());
   }

   /**
    * Finds an interval by integer number
    * @param interval integer value
    * @return interval
    */
   public static Interval findIntervalByNumber(int interval) {
      switch (interval) {
         case 0: return Interval.PERFECT_UNISON;
         case 1: return Interval.MINOR_SECOND;
         case 2: return Interval.MAJOR_SECOND;
         case 3: return Interval.MINOR_THIRD;
         case 4: return Interval.MAJOR_THIRD;
         case 5: return Interval.PERFECT_FOURTH;
         case 6: return Interval.TRITONE;
         case 7: return Interval.PERFECT_FIFTH;
         case 8: return Interval.MINOR_SIXTH;
         case 9: return Interval.MAJOR_SIXTH;
         case 10: return Interval.MINOR_SEVENTH;
         case 11: return Interval.MAJOR_SEVENTH;
         case 12: return Interval.OCTAVE;
         case 13: return Interval.MINOR_NINTH;
         case 14: return Interval.MAJOR_NINTH;
         case 15: return Interval.MINOR_TENTH;
         case 16: return Interval.MAJOR_TENTH;
         case 17: return Interval.PERFECT_ELEVENTH;
         case 18: return Interval.OCTAVE_PLUS_TRITONE;
         case 19: return Interval.PERFECT_TWELFTH;
         case 20: return Interval.MINOR_THIRTEENTH;
         case 21: return Interval.MAJOR_THIRTEENTH;
         case 22: return Interval.MINOR_FOURTEENTH;
         case 23: return Interval.MAJOR_FOURTEENTH;
         case 24: return Interval.DOUBLE_OCTAVE;
      }
      if(interval > 24) return Interval.MORE_THAN_DOUBLE_OCTAVE;
      else return null;
   }

   /**
    * Finds pitch of note by integer number
    * @param pitch integer value
    * @return pitch
    */
   public static PitchEnum findPitchByNumber(int pitch) {
      switch (pitch) {
         case 0: return PitchEnum.C0;
         case 1: return PitchEnum.Ch0;
         case 2: return PitchEnum.D0;
         case 3: return PitchEnum.Dh0;
         case 4: return PitchEnum.E0;
         case 5: return PitchEnum.F0;
         case 6: return PitchEnum.Fh0;
         case 7: return PitchEnum.G0;
         case 8: return PitchEnum.Gh0;
         case 9: return PitchEnum.A0;
         case 10: return PitchEnum.Ah0;
         case 11: return PitchEnum.B0;
         case 12: return PitchEnum.C1;
         case 13: return PitchEnum.Ch1;
         case 14: return PitchEnum.D1;
         case 15: return PitchEnum.Dh1;
         case 16: return PitchEnum.E1;
         case 17: return PitchEnum.F1;
         case 18: return PitchEnum.Fh1;
         case 19: return PitchEnum.G1;
         case 20: return PitchEnum.Gh1;
         case 21: return PitchEnum.A1;
         case 22: return PitchEnum.Ah1;
         case 23: return PitchEnum.B1;
         case 24: return PitchEnum.C2;
         case 25: return PitchEnum.Ch2;
         case 26: return PitchEnum.D2;
         case 27: return PitchEnum.Dh2;
         case 28: return PitchEnum.E2;
         case 29: return PitchEnum.F2;
         case 30: return PitchEnum.Fh2;
         case 31: return PitchEnum.G2;
         case 32: return PitchEnum.Gh2;
         case 33: return PitchEnum.A2;
         case 34: return PitchEnum.Ah2;
         case 35: return PitchEnum.B2;
         case 36: return PitchEnum.C3;
         case 37: return PitchEnum.Ch3;
         case 38: return PitchEnum.D3;
         case 39: return PitchEnum.Dh3;
         case 40: return PitchEnum.E3;
         case 41: return PitchEnum.F3;
         case 42: return PitchEnum.Fh3;
         case 43: return PitchEnum.G3;
         case 44: return PitchEnum.Gh3;
         case 45: return PitchEnum.A3;
         case 46: return PitchEnum.Ah3;
         case 47: return PitchEnum.B3;
         case 48: return PitchEnum.C4;
         case 49: return PitchEnum.Ch4;
         case 50: return PitchEnum.D4;
         case 51: return PitchEnum.Dh4;
         case 52: return PitchEnum.E4;
         case 53: return PitchEnum.F4;
         case 54: return PitchEnum.Fh4;
         case 55: return PitchEnum.G4;
         case 56: return PitchEnum.Gh4;
         case 57: return PitchEnum.A4;
         case 58: return PitchEnum.Ah4;
         case 59: return PitchEnum.B4;
         case 60: return PitchEnum.C5;
         case 61: return PitchEnum.Ch5;
         case 62: return PitchEnum.D5;
         case 63: return PitchEnum.Dh5;
         case 64: return PitchEnum.E5;
         case 65: return PitchEnum.F5;
         case 66: return PitchEnum.Fh5;
         case 67: return PitchEnum.G5;
         case 68: return PitchEnum.Gh5;
         case 69: return PitchEnum.A5;
         case 70: return PitchEnum.Ah5;
         case 71: return PitchEnum.B5;
         case 72: return PitchEnum.C6;
         case 73: return PitchEnum.Ch6;
         case 74: return PitchEnum.D6;
         case 75: return PitchEnum.Dh6;
         case 76: return PitchEnum.E6;
         case 77: return PitchEnum.F6;
         case 78: return PitchEnum.Fh6;
         case 79: return PitchEnum.G6;
         case 80: return PitchEnum.Gh6;
         case 81: return PitchEnum.A6;
         case 82: return PitchEnum.Ah6;
         case 83: return PitchEnum.B6;
         case 84: return PitchEnum.C7;
         case 85: return PitchEnum.Ch7;
         case 86: return PitchEnum.D7;
         case 87: return PitchEnum.Dh7;
         case 88: return PitchEnum.E7;
         case 89: return PitchEnum.F7;
         case 90: return PitchEnum.Fh7;
         case 91: return PitchEnum.G7;
         case 92: return PitchEnum.Gh7;
         case 93: return PitchEnum.A7;
         case 94: return PitchEnum.Ah7;
         case 95: return PitchEnum.B7;
         case 96: return PitchEnum.C8;
         case 97: return PitchEnum.Ch8;
         case 98: return PitchEnum.D8;
         case 99: return PitchEnum.Dh8;
         case 100: return PitchEnum.E8;
         case 101: return PitchEnum.F8;
         case 102: return PitchEnum.Fh8;
         case 103: return PitchEnum.G8;
         case 104: return PitchEnum.Gh8;
         case 105: return PitchEnum.A8;
         case 106: return PitchEnum.Ah8;
         case 107: return PitchEnum.B8;
         case 108: return PitchEnum.C9;
         case 109: return PitchEnum.Ch9;
         case 110: return PitchEnum.D9;
         case 111: return PitchEnum.Dh9;
         case 112: return PitchEnum.E9;
         case 113: return PitchEnum.F9;
         case 114: return PitchEnum.Fh9;
         case 115: return PitchEnum.G9;
         case 116: return PitchEnum.Gh9;
         case 117: return PitchEnum.A9;
         case 118: return PitchEnum.Ah9;
         case 119: return PitchEnum.B9;
         case 120: return PitchEnum.C10;
         case 121: return PitchEnum.Ch10;
         case 122: return PitchEnum.D10;
         case 123: return PitchEnum.Dh10;
         case 124: return PitchEnum.E10;
         case 125: return PitchEnum.F10;
         case 126: return PitchEnum.Fh10;
         case 127: return PitchEnum.G10;
         case 128: return PitchEnum.Gh10;
         case 129: return PitchEnum.A10;
         case 130: return PitchEnum.Ah10;
         case 131: return PitchEnum.B10;
         case 132: return PitchEnum.C11;
      }
      return null;
   }
}
