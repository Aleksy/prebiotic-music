package aleksy.prebioticmusic.archean.musicmodel.builder;

import aleksy.prebioticmusic.archean.musicmodel.enumerate.PitchEnum;
import aleksy.prebioticmusic.archean.musicmodel.model.Music;
import aleksy.prebioticmusic.archean.musicmodel.model.Note;
import aleksy.prebioticmusic.archean.musicmodel.model.impl.StandardMusic;
import aleksy.prebioticmusic.archean.musicmodel.model.impl.StandardNote;

import java.util.ArrayList;
import java.util.List;

/**
 * Music builder
 */
public class MusicBuilder {
   private List<Note> notes;

   /**
    * Constructor
    */
   public MusicBuilder() {
      notes = new ArrayList<>();
   }

   /**
    * Adds new note to the music
    * @param pitchEnum of note
    * @param startPoint of note
    * @param duration of note
    * @return builder
    */
   public MusicBuilder addNote(PitchEnum pitchEnum, int startPoint, int duration) {
      notes.add(new StandardNote(pitchEnum, startPoint, duration));
      return this;
   }

   /**
    * Creates new instance of {@link Music} interface
    * @param measuresPerBar of music
    * @param cellsPerMeasure of music
    * @return new instance of music
    */
   public Music create(int measuresPerBar, int cellsPerMeasure) {
      return new StandardMusic(notes, measuresPerBar, cellsPerMeasure);
   }
}
