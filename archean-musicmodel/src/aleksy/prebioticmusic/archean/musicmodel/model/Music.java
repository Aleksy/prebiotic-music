package aleksy.prebioticmusic.archean.musicmodel.model;

import java.util.List;

/**
 * Music matrix interface
 */
public interface Music {
   /**
    * Getter for amount of measures per one bar
    * @return measures per bar
    */
   int getMeasuresPerBar();

   /**
    * Getter for resolution (amount of single-cells in the one bar)
    * @return resolution
    */
   int getResolution();

   /**
    * Getter for amount of bars
    * @return amount of bars
    */
   int getAmountOfBars();

   /**
    * Amount of notes
    * @return amount of notes
    */
   int size();

   /**
    * Calculates the ambitus of all notes
    * @return ambitus
    */
   int calculateAmbitus();

   /**
    * Getter for list of notes
    * @return notes
    */
   List<Note> getNotes();

   /**
    * Returns amount of single-cells per one measure
    * @return single-cells per measure
    */
   int getCellsPerMeasure();

   /**
    * Getter for the lowest note of the music
    * @return the lowest note
    */
   Note getLowestNote();

   /**
    * Getter for the highest note of the music
    * @return the highest note
    */
   Note getHighestNote();

   /**
    * Getter for scale identifier of the music
    * @return scale identifier
    */
   ScaleIdentifier getScaleIdentifier();
}
