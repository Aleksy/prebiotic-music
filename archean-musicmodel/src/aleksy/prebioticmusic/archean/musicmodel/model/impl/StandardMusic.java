package aleksy.prebioticmusic.archean.musicmodel.model.impl;

import aleksy.prebioticmusic.archean.musicmodel.enumerate.Key;
import aleksy.prebioticmusic.archean.musicmodel.enumerate.Scale;
import aleksy.prebioticmusic.archean.musicmodel.logic.MusicLogic;
import aleksy.prebioticmusic.archean.musicmodel.logic.NoteLogic;
import aleksy.prebioticmusic.archean.musicmodel.model.Music;
import aleksy.prebioticmusic.archean.musicmodel.model.Note;
import aleksy.prebioticmusic.archean.musicmodel.model.ScaleIdentifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Standard {@link Music} implementation
 */
public class StandardMusic implements Music {
   private List<Note> notes;
   private int measuresPerBar;
   private int cellsPerMeasure;

   public StandardMusic(List<Note> notes, int measuresPerBar, int cellsPerMeasure) {
      this.notes = notes;
      this.measuresPerBar = measuresPerBar;
      this.cellsPerMeasure = cellsPerMeasure;
   }

   @Override
   public int getMeasuresPerBar() {
      return measuresPerBar;
   }

   @Override
   public int getResolution() {
      return measuresPerBar * cellsPerMeasure;
   }

   @Override
   public int getAmountOfBars() {
      int farthestEndOfNote = 0;
      for(Note note : notes) {
         int endOfNote = note.getDuration() + note.getStartPoint();
         if(endOfNote > farthestEndOfNote)
            farthestEndOfNote = endOfNote;
      }
      int amountOfBars = 0;
      int resolution = getResolution();
      while (resolution * amountOfBars < farthestEndOfNote) {
         amountOfBars++;
      }
      return amountOfBars;
   }

   @Override
   public int size() {
      return notes.size();
   }

   @Override
   public int calculateAmbitus() {
      return NoteLogic.calculateInterval(getLowestNote(), getHighestNote());
   }

   @Override
   public List<Note> getNotes() {
      return notes;
   }

   @Override
   public int getCellsPerMeasure() {
      return cellsPerMeasure;
   }

   @Override
   public Note getLowestNote() {
      Note lowest = notes.get(0);
      for(Note note : notes) {
         if(note.getPitch().getPitchInteger() < lowest.getPitch().getPitchInteger()) {
            lowest = note;
         }
      }
      return lowest;
   }

   @Override
   public Note getHighestNote() {
      Note highest = notes.get(0);
      for(Note note : notes) {
         if(note.getPitch().getPitchInteger() > highest.getPitch().getPitchInteger()) {
            highest = note;
         }
      }
      return highest;
   }

   @Override
   public ScaleIdentifier getScaleIdentifier() {
      List<Key> usedNotes = MusicLogic.getUsedNotes(this);
      List<Integer> pattern = createPattern(usedNotes);
      double nearestDistance = Double.MAX_VALUE;
      Scale nearestScale = null;
      int nearestKey = 0;
      for(Scale scale : Scale.values()) {
         double distance = 0.0;
         int keyInteger = 0;
         for(int i = 0; i < pattern.size(); i++) {
            for(int j = 0; j < scale.getPattern().size(); j++) {
               distance += Math.pow(scale.getPattern().get(j) - pattern.get(j), 2);
            }
            pattern = rotatePatternUp(pattern);
            keyInteger++;
            distance = Math.sqrt(distance);
            if(nearestDistance > distance) {
               nearestDistance = distance;
               nearestScale = scale;
               nearestKey = keyInteger;
            }
         }
      }
      return new ScaleIdentifier(nearestDistance,
         nearestScale, NoteLogic.findPitchByNumber(nearestKey).getKey());
   }
   @Override
   public String toString() {
      ScaleIdentifier scaleIdentifier = getScaleIdentifier();
      return "Music:\namount of notes: " + size()
         + "\nmeasures per bar: " + measuresPerBar
         + "\ncells per measure: " + cellsPerMeasure
         + "\nresolution: " + getResolution()
         + "\namount of bars: " + getAmountOfBars()
         + "\nambitus: " + NoteLogic.findIntervalByNumber(calculateAmbitus())
         + "\nmatched scale: " + scaleIdentifier.getNearestScale()
         + "\nsimilarity: " + scaleIdentifier.getSimilarity();
   }

   private List<Integer> createPattern(List<Key> notes) {
      Integer[] integers = new Integer[12];
      for(int i = 0; i < integers.length; i++)
         integers[i] = 0;
      for(Key key : notes) {
         switch(key) {
            case C: integers[0] = 1;
            case Ch: integers[1] = 1;
            case D: integers[2] = 1;
            case Dh: integers[3] = 1;
            case E: integers[4] = 1;
            case F: integers[5] = 1;
            case Fh: integers[6] = 1;
            case G: integers[7] = 1;
            case Gh: integers[8] = 1;
            case A: integers[9] = 1;
            case Ah: integers[10] = 1;
            case B: integers[11] = 1;
         }
      }
      return new ArrayList<>(Arrays.asList(integers));
   }

   private List<Integer> rotatePatternUp(List<Integer> pattern) {
      Integer first = pattern.get(0);
      pattern.remove(0);
      pattern.add(first);
      return pattern;
   }
}
