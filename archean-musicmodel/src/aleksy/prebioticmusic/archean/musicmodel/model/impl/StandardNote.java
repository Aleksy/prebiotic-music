package aleksy.prebioticmusic.archean.musicmodel.model.impl;

import aleksy.prebioticmusic.archean.musicmodel.enumerate.PitchEnum;
import aleksy.prebioticmusic.archean.musicmodel.model.Note;

/**
 * Standard {@link Note} implementation
 */
public class StandardNote implements Note {
   private PitchEnum pitch;
   private int startPoint;
   private int duration;

   public StandardNote(PitchEnum pitch, int startPoint, int duration) {
      this.pitch = pitch;
      this.startPoint = startPoint;
      this.duration = duration;
   }

   @Override
   public PitchEnum getPitch() {
      return pitch;
   }

   @Override
   public int getStartPoint() {
      return startPoint;
   }

   @Override
   public int getDuration() {
      return duration;
   }
}
