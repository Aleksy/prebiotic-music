package aleksy.prebioticmusic.archean.musicmodel.model;

import aleksy.prebioticmusic.archean.musicmodel.enumerate.Key;
import aleksy.prebioticmusic.archean.musicmodel.enumerate.Scale;

public class ScaleIdentifier {
   private double similarity;
   private Scale nearestScale;
   private Key key;

   public ScaleIdentifier(double similarity, Scale nearestScale, Key key) {
      this.similarity = similarity;
      this.nearestScale = nearestScale;
      this.key = key;
   }

   public double getSimilarity() {
      return similarity;
   }

   public Scale getNearestScale() {
      return nearestScale;
   }

   public Key getKey() {
      return key;
   }
}

