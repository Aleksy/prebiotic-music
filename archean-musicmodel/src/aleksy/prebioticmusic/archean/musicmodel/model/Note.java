package aleksy.prebioticmusic.archean.musicmodel.model;

import aleksy.prebioticmusic.archean.musicmodel.enumerate.PitchEnum;

/**
 * Note interface
 */
public interface Note {
   /**
    * Getter for pitch of the note
    * @return pitch
    */
   PitchEnum getPitch();

   /**
    * Getter for start point of the note
    * @return start point
    */
   int getStartPoint();

   /**
    * Getter for duration of note
    * @return duration
    */
   int getDuration();
}
