package aleksy.prebioticmusic.archean.musicmodel.io;

import aleksy.prebioticmusic.archean.musicmodel.enumerate.PitchEnum;
import aleksy.prebioticmusic.archean.musicmodel.model.Music;
import aleksy.prebioticmusic.archean.musicmodel.model.Note;
import aleksy.prebioticmusic.archean.musicmodel.model.impl.StandardMusic;
import aleksy.prebioticmusic.archean.musicmodel.model.impl.StandardNote;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Archean music input-output handler
 */
public class ArcheanMusicIO {
   /**
    * Writes the {@link Music} instance to the file
    * @param music to write
    * @param path of the file
    * @throws IOException when is the problem with the file
    */
   public static void write(Music music, String path) throws IOException {
      File file = new File(path);
      if (!file.exists()) {
         file.getParentFile().mkdirs();
         file.createNewFile();
      }
      FileWriter fileWriter = new FileWriter(file);
      fileWriter.write(music.getCellsPerMeasure() + "\n");
      fileWriter.write(music.getMeasuresPerBar() + "\n");
      for (Note note : music.getNotes()) {
         fileWriter.write(note.getStartPoint() + "\n");
         fileWriter.write(note.getDuration() + "\n");
         fileWriter.write(note.getPitch().toString() + "\n");
      }
      fileWriter.close();
   }

   /**
    * Reads the {@link Music} from the file
    * @param path of the file
    * @return {@link Music} instance
    * @throws FileNotFoundException when file wasn't found
    */
   public static Music read(String path) throws FileNotFoundException {
      File file = new File(path);
      if (file.exists()) {
         Scanner scanner = new Scanner(file);
         int cellsPerMeasure = Integer.parseInt(scanner.nextLine());
         int measuresPerBar = Integer.parseInt(scanner.nextLine());
         List<Note> notes = new ArrayList<>();
         while (scanner.hasNext()) {
            int startPoint = Integer.parseInt(scanner.nextLine());
            int duration = Integer.parseInt(scanner.nextLine());
            PitchEnum pitch = PitchEnum.valueOf(scanner.nextLine());
            notes.add(new StandardNote(pitch, startPoint, duration));
         }
         scanner.close();
         return new StandardMusic(notes, measuresPerBar, cellsPerMeasure);
      }
      return null;
   }
}
