package aleksy.prebioticmusic.archean.musicmodel.enumerate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Scale of the melody
 */
public enum Scale {
   NATURAL_MAJOR(1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1),
   NATURAL_MINOR(1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0),
   PENTATONIX(1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0);

   private List<Integer> pattern;

   Scale(Integer ... integers) {
      pattern = new ArrayList<>(Arrays.asList(integers));
   }

   public List<Integer> getPattern() {
      return pattern;
   }
}
