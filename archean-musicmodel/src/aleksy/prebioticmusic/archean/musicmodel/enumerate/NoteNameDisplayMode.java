package aleksy.prebioticmusic.archean.musicmodel.enumerate;

/**
 * Note name display mode
 */
public enum NoteNameDisplayMode {
   /**
    * American naming convention
    */
   AMERICAN,
   /**
    * European naming convention
    */
   EUROPEAN
}
