package aleksy.prebioticmusic.archean.musicmodel.enumerate;

/**
 * Pitch enumerate class
 */
public enum PitchEnum {
   C0(0, "C0", "C0", Key.C),
   Ch0(1, "C#0", "Cis0", Key.Ch),
   D0(2, "D0", "D0", Key.D),
   Dh0(3, "D#0", "Dis0", Key.Dh),
   E0(4, "E0", "E0", Key.E),
   F0(5, "F0", "F0", Key.F),
   Fh0(6, "F#0", "Fis0", Key.Fh),
   G0(7, "G0", "G0", Key.G),
   Gh0(8, "G#0", "Gis0", Key.Gh),
   A0(9, "A0", "A0", Key.A),
   Ah0(10, "A#0", "Ais0", Key.Ah),
   B0(11, "B0", "H0", Key.B),
   C1(12, "C1", "C1", Key.C),
   Ch1(13, "C#1", "Cis1", Key.Ch),
   D1(14, "D1", "D1", Key.D),
   Dh1(15, "D#1", "Dis1", Key.Dh),
   E1(16, "E1", "E1", Key.E),
   F1(17, "F1", "F1", Key.F),
   Fh1(18, "F#1", "Fis1", Key.Fh),
   G1(19, "G1", "G1", Key.G),
   Gh1(20, "G#1", "Gis1", Key.Gh),
   A1(21, "A1", "A1", Key.A),
   Ah1(22, "A#1", "Ais1", Key.Ah),
   B1(23, "B1", "H1", Key.B),
   C2(24, "C2", "C2", Key.C),
   Ch2(25, "C#2", "Cis2", Key.Ch),
   D2(26, "D2", "D2", Key.D),
   Dh2(27, "D#2", "Dis2", Key.Dh),
   E2(28, "E2", "E2", Key.E),
   F2(29, "F2", "F2", Key.F),
   Fh2(30, "F#2", "Fis2", Key.Fh),
   G2(31, "G2", "G2", Key.G),
   Gh2(32, "G#2", "Gis2", Key.Gh),
   A2(33, "A2", "A2", Key.A),
   Ah2(34, "A#2", "Ais2", Key.Ah),
   B2(35, "B2", "H2", Key.B),
   C3(36, "C3", "C3", Key.C),
   Ch3(37, "C#3", "Cis3", Key.Ch),
   D3(38, "D3", "D3", Key.D),
   Dh3(39, "D#3", "Dis3", Key.Dh),
   E3(40, "E3", "E3", Key.E),
   F3(41, "F3", "F3", Key.F),
   Fh3(42, "F#3", "Fis3", Key.Fh),
   G3(43, "G3", "G3", Key.G),
   Gh3(44, "G#3", "Gis3", Key.Gh),
   A3(45, "A3", "A3", Key.A),
   Ah3(46, "A#3", "Ais3", Key.Ah),
   B3(47, "B3", "H3", Key.B),
   C4(48, "C4", "C4", Key.C),
   Ch4(49, "C#4", "Cis4", Key.Ch),
   D4(50, "D4", "D4", Key.D),
   Dh4(51, "D#4", "Dis4", Key.Dh),
   E4(52, "E4", "E4", Key.E),
   F4(53, "F4", "F4", Key.F),
   Fh4(54, "F#4", "Fis4", Key.Fh),
   G4(55, "G4", "G4", Key.G),
   Gh4(56, "G#4", "Gis4", Key.Gh),
   A4(57, "A4", "A4", Key.A),
   Ah4(58, "A#4", "Ais4", Key.Ah),
   B4(59, "B4", "H4", Key.B),
   C5(60, "C5", "C5", Key.C),
   Ch5(61, "C#5", "Cis5", Key.Ch),
   D5(62, "D5", "D5", Key.D),
   Dh5(63, "D#5", "Dis5", Key.Dh),
   E5(64, "E5", "E5", Key.E),
   F5(65, "F5", "F5", Key.F),
   Fh5(66, "F#5", "Fis5", Key.Fh),
   G5(67, "G5", "G5", Key.G),
   Gh5(68, "G#5", "Gis5", Key.Gh),
   A5(69, "A5", "A5", Key.A),
   Ah5(70, "A#5", "Ais5", Key.Ah),
   B5(71, "B5", "H5", Key.B),
   C6(72, "C6", "C6", Key.C),
   Ch6(73, "C#6", "Cis6", Key.Ch),
   D6(74, "D6", "D6", Key.D),
   Dh6(75, "D#6", "Dis6", Key.Dh),
   E6(76, "E6", "E6", Key.E),
   F6(77, "F6", "F6", Key.F),
   Fh6(78, "F#6", "Fis6", Key.Fh),
   G6(79, "G6", "G6", Key.G),
   Gh6(80, "G#6", "Gis6", Key.Gh),
   A6(81, "A6", "A6", Key.A),
   Ah6(82, "A#6", "Ais6", Key.Ah),
   B6(83, "B6", "H6", Key.B),
   C7(84, "C7", "C7", Key.C),
   Ch7(85, "C#7", "Cis7", Key.Ch),
   D7(86, "D7", "D7", Key.D),
   Dh7(87, "D#7", "Dis7", Key.Dh),
   E7(88, "E7", "E7", Key.E),
   F7(89, "F7", "F7", Key.F),
   Fh7(90, "F#7", "Fis7", Key.Fh),
   G7(91, "G7", "G7", Key.G),
   Gh7(92, "G#7", "Gis7", Key.Gh),
   A7(93, "A7", "A7", Key.A),
   Ah7(94, "A#7", "Ais7", Key.Ah),
   B7(95, "B7", "H7", Key.B),
   C8(96, "C8", "C8", Key.C),
   Ch8(97, "C#8", "Cis8", Key.Ch),
   D8(98, "D8", "D8", Key.D),
   Dh8(99, "D#8", "Dis8", Key.Dh),
   E8(100, "E8", "E8", Key.E),
   F8(101, "F8", "F8", Key.F),
   Fh8(102, "F#8", "Fis8", Key.Fh),
   G8(103, "G8", "G8", Key.G),
   Gh8(104, "G#8", "Gis8", Key.Gh),
   A8(105, "A8", "A8", Key.A),
   Ah8(106, "A#8", "Ais8", Key.Ah),
   B8(107, "B8", "H8", Key.B),
   C9(108, "C9", "C9", Key.C),
   Ch9(109, "C#9", "Cis9", Key.Ch),
   D9(110, "D9", "D9", Key.D),
   Dh9(111, "D#9", "Dis9", Key.Dh),
   E9(112, "E9", "E9", Key.E),
   F9(113, "F9", "F9", Key.F),
   Fh9(114, "F#9", "Fis9", Key.Fh),
   G9(115, "G9", "G9", Key.G),
   Gh9(116, "G#9", "Gis9", Key.Gh),
   A9(117, "A9", "A9", Key.A),
   Ah9(118, "A#9", "Ais9", Key.Ah),
   B9(119, "B9", "H9", Key.B),
   C10(120, "C10", "C10", Key.C),
   Ch10(121, "C#10", "Cis10", Key.Ch),
   D10(122, "D10", "D10", Key.D),
   Dh10(123, "D#10", "Dis10", Key.Dh),
   E10(124, "E10", "E10", Key.E),
   F10(125, "F10", "F10", Key.F),
   Fh10(126, "F#10", "Fis10", Key.Fh),
   G10(127, "G10", "G10", Key.G),
   Gh10(128, "G#10", "Gis10", Key.Gh),
   A10(129, "A10", "A10", Key.A),
   Ah10(130, "A#10", "Ais10", Key.Ah),
   B10(131, "B10", "H10", Key.B),
   C11(132, "C11", "C11", Key.C);

   private int pitchInteger;
   private String pitchAmericanString;
   private String pitchEuropeanString;
   private Key key;

   PitchEnum(int pitchInteger, String pitchAmericanString, String pitchEuropeanString, Key key) {
      this.pitchInteger = pitchInteger;
      this.pitchAmericanString = pitchAmericanString;
      this.pitchEuropeanString = pitchEuropeanString;
      this.key = key;
   }

   /**
    * Getter for pitch integer value
    *
    * @return absolute pitch integer value of the note
    */
   public int getPitchInteger() {
      return pitchInteger;
   }

   /**
    * Pitch name in the american convention
    *
    * @return pitch name
    */
   public String getPitchAmericanString() {
      return pitchAmericanString;
   }

   /**
    * Pitch name in the european convention
    *
    * @return pitch name
    */
   public String getPitchEuropeanString() {
      return pitchEuropeanString;
   }

   public Key getKey() {
      return key;
   }
}
