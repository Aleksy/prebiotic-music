package aleksy.prebioticmusic.archean.musicmodel.enumerate;

/**
 * Key of the note
 */
public enum Key {
   C,
   Ch,
   D,
   Dh,
   E,
   F,
   Fh,
   G,
   Gh,
   A,
   Ah,
   B,
}
