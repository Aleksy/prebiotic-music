package aleksy.prebioticmusic.archean.musicmodel.enumerate;

/**
 * Interval enumerate class
 */
public enum Interval {
   PERFECT_UNISON(0),
   MINOR_SECOND(1),
   MAJOR_SECOND(2),
   MINOR_THIRD(3),
   MAJOR_THIRD(4),
   PERFECT_FOURTH(5),
   TRITONE(6),
   PERFECT_FIFTH(7),
   MINOR_SIXTH(8),
   MAJOR_SIXTH(9),
   MINOR_SEVENTH(10),
   MAJOR_SEVENTH(11),
   OCTAVE(12),
   MINOR_NINTH(13),
   MAJOR_NINTH(14),
   MINOR_TENTH(15),
   MAJOR_TENTH(16),
   PERFECT_ELEVENTH(17),
   OCTAVE_PLUS_TRITONE(18),
   PERFECT_TWELFTH(19),
   MINOR_THIRTEENTH(20),
   MAJOR_THIRTEENTH(21),
   MINOR_FOURTEENTH(22),
   MAJOR_FOURTEENTH(23),
   DOUBLE_OCTAVE(24),
   MORE_THAN_DOUBLE_OCTAVE(-1);

   private Integer interval;

   Interval(Integer interval) {
      this.interval = interval;
   }

   /**
    * Getter for integer representation of the interval
    * @return interval value
    */
   public Integer getInterval() {
      return interval;
   }
}
