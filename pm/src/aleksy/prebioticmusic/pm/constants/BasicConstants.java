package aleksy.prebioticmusic.pm.constants;

/**
 * Basic constants class
 */
public class BasicConstants {
   /**
    * Name of program
    */
   public static final String NAME = "Prebiotic Music";
   /**
    * Version
    */
   public static final String VERSION = "1.0.0";
   /**
    * Author
    */
   public static final String AUTHOR = "Aleksy Bernat, Wroclaw";
   /**
    * Command name
    */
   public static final String COMMAND_NAME = "pm";
}
