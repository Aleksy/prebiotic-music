package aleksy.prebioticmusic.pm.constants;

/**
 * Technical constants class
 */
public class TechnicalConstants {
   /**
    * Info file name
    */
   public static final String INFO_FILE_NAME = "pm.info";
}
