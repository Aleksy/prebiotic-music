package aleksy.prebioticmusic.pm.model;

import aleksy.prebioticmusic.prebioticcommandapplicationengine.annotation.Command;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.logic.Logger;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.model.AbstractCommand;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * Check command
 */
@Command(name = "check", description = "checks if the directory has initialized by any pm-realization")
public class CheckCommand extends AbstractCommand {
   @Override
   protected String displayHelp() {
      return "Command checks if the directory has initialized by any pm-realization";
   }

   @Override
   protected void run() {
      File pm = new File(getCallDirectory() + "\\pm");
      if(!pm.exists()) {
         Logger.log("This directory is not initialized by any prebiotic-music realization");
      }
      else {
         Scanner scanner;
         try {
            scanner = new Scanner(pm);
            String pmInfo = scanner.nextLine();
            if(!pmInfo.startsWith("pm:")) {
               Logger.log("This directory could be initialized by prebiotic-music realization, but" +
                  " pm file format is wrong");
            }
            else
               Logger.log("This directory is initialized by " + pmInfo.substring(3) + " realization");
            scanner.close();
         } catch (FileNotFoundException e) {
            Logger.log(e.getMessage());
         }
      }
   }
}
