package aleksy.prebioticmusic.pm;

import aleksy.prebioticmusic.pm.constants.BasicConstants;
import aleksy.prebioticmusic.pm.model.CheckCommand;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.exception.PcaeException;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.logic.Logger;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.runner.PcaeRunner;

/**
 * Main application class
 */
public class PmApp {

    /**
     * Main method
     * @param args of the program
     */
    public static void main(String[] args) {
        PcaeRunner pcaeRunner = new PcaeRunner();
        pcaeRunner.setRealizationName(BasicConstants.NAME);
        pcaeRunner.setRealizationVersion(BasicConstants.VERSION);
        pcaeRunner.setRealizationAuthor(BasicConstants.AUTHOR);
        pcaeRunner.setRealizationCommandName(BasicConstants.COMMAND_NAME);
        pcaeRunner.addCommand(CheckCommand.class);
        try {
            pcaeRunner.run(args);
        } catch (InstantiationException | IllegalAccessException | PcaeException e) {
            Logger.log(e.getMessage());
        }
    }
}
