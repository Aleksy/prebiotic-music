# PM - Prebiotic Music status checker

This simple program is one of programs created as part of the Prebiotic Music project.
Version 1.0.0 has one method which can check if the directory is initialized by
any Prebiotic Music Realization:

```
pm check
```

# Installation

Download the program [here](https://drive.google.com/open?id=1ZxXU3tSEal4ltVDXkSqIjH4W9tw9my-x), and read instruction below (for Windows):   
1. Press WINDOW + R and type
```
%appdata%
```
2. Create the folder "prebiotic-music" if you don't do that earlier.   
3. Extract file pm.bat and pm.jar from the ZIP archive here.   
4. If you do not do that earlier, add this directory to your PATH variable by calling
```
set PATH=%PATH%;C:\Users\<YOUR_USERNAME>\AppData\Roaming\prebiotic-music
```
in the cmd. Type your user name instead of YOUR_USERNAME.   
5. You can check if everything is done by typing:
```
pm info
```
in the cmd.