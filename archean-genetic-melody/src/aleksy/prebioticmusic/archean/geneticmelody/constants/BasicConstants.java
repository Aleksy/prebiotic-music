package aleksy.prebioticmusic.archean.geneticmelody.constants;

public class BasicConstants {
   public static final String NAME = "Archean Genetic Melody";
   public static final String VERSION = "0.0.2";
   public static final String AUTHOR = "Aleksy Bernat, Wroclaw";
   public static final String COMMAND = "archgem";
}
