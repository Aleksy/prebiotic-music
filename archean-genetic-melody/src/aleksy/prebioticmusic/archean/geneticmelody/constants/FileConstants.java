package aleksy.prebioticmusic.archean.geneticmelody.constants;

public class FileConstants {
   public static final String CONFIG_FILE_NAME = "gen_alg.config";
   public static final String CONFIG_FILE_DEFAULT_CONTENT
      = "generations=1000" +
      "\r\npopulation=500" +
      "\r\nselection_mode=ROULETTE" +
      "\r\nmutation_chance=0.01\n";
}
