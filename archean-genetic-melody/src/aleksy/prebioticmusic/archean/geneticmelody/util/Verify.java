package aleksy.prebioticmusic.archean.geneticmelody.util;

import aleksy.prebioticmusic.archean.geneticmelody.constants.BasicConstants;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Verify {
   public static boolean initialization(String path) {
      File pm = new File(path + "\\pm");
      if (pm.exists()) {
         try {
            Scanner scanner = new Scanner(pm);
            String line = scanner.nextLine();
            scanner.close();
            return line.endsWith(BasicConstants.COMMAND);
         } catch (FileNotFoundException ignored) {
         }
      }
      return false;
   }
}
