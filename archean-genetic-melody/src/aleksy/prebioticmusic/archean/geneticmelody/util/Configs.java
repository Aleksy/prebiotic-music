package aleksy.prebioticmusic.archean.geneticmelody.util;

import aleksy.prebioticmusic.archean.geneticmelody.constants.BasicConstants;
import aleksy.prebioticmusic.archean.geneticmelody.constants.FileConstants;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 */
public class Configs {
   public static String readConfigFile(String directory) throws FileNotFoundException {StringBuilder contentOfFile = new StringBuilder();
      File file = new File(directory + "\\."
         + BasicConstants.COMMAND + "\\" + FileConstants.CONFIG_FILE_NAME);
      Scanner scanner = new Scanner(file);
      while (scanner.hasNextLine())
         contentOfFile.append(scanner.nextLine()).append("\n");
      scanner.close();
      return contentOfFile.toString();
   }

   public static Map<String, String> generateMapFromConfigs(String configs) {
      String generationsString = configs.substring(configs.indexOf("generations=") + "generations=".length(),
         configs.indexOf("population") - 1);
      String populationString = configs.substring(configs.indexOf("population=") + "population=".length(),
         configs.indexOf("selection_mode") - 1);
      String selectionModeString = configs.substring(configs.indexOf("selection_mode=") + "selection_mode=".length(),
         configs.indexOf("mutation_chance") - 1);
      String mutationChanceString = configs.substring(configs.indexOf("mutation_chance=") + "mutation_chance=".length(),
         configs.length() - 1);
      Map<String, String> map = new HashMap<>();
      map.put("generations", generationsString);
      map.put("population", populationString);
      map.put("selection_mode", selectionModeString);
      map.put("mutation_chance", mutationChanceString);
      return map;
   }
}
