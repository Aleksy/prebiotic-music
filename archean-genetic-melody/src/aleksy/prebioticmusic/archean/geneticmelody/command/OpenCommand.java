package aleksy.prebioticmusic.archean.geneticmelody.command;

import aleksy.prebioticmusic.archean.geneticmelody.constants.BasicConstants;
import aleksy.prebioticmusic.archean.geneticmelody.util.Verify;
import aleksy.prebioticmusic.archean.musicmodel.display.MusicPanel;
import aleksy.prebioticmusic.archean.musicmodel.enumerate.NoteNameDisplayMode;
import aleksy.prebioticmusic.archean.musicmodel.io.ArcheanMusicIO;
import aleksy.prebioticmusic.archean.musicmodel.model.Music;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.annotation.Command;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.logic.Logger;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.model.AbstractCommand;

import javax.swing.*;
import java.io.FileNotFoundException;

@Command(name = "open", description = "opens the file")
public class OpenCommand extends AbstractCommand {
   @Override
   protected String displayHelp() {
      return "opens the file in window. Arguments:" +
         "\nf - file to open" +
         "\nhue - hue of displayed notes in the frame" +
         "\nn - name of notes (AMERICAN or EUROPEAN)" +
         "\ntms - amount of single steps displayed in the frame" +
         "\nExample: " + BasicConstants.COMMAND + " " + getCommand() + " :f file.txt";
   }

   @Override
   protected void run() {
      try {
         if (Verify.initialization(getCallDirectory())) {
            Music music = ArcheanMusicIO.read(getCallDirectory() + "\\" + getArguments().get("f"));
            JFrame frame = new JFrame(BasicConstants.NAME + " " + BasicConstants.VERSION);
            System.out.println(music);
            MusicPanel musicPanel = new MusicPanel(music,
               getArguments().get("hue") == null ? 0.45 : Double.parseDouble(getArguments().get("hue")),
               getArguments().get("n") == null ? NoteNameDisplayMode.AMERICAN : NoteNameDisplayMode.valueOf(getArguments().get("n")),
               getArguments().get("tms") == null ? 40 : Integer.parseInt(getArguments().get("tms")));
            frame.setContentPane(musicPanel);
            frame.addMouseListener(musicPanel);
            frame.setFocusable(true);
            frame.setVisible(true);
            frame.setSize(800, 200);
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
         } else {
            Logger.log(BasicConstants.COMMAND + " is not initialized here.");
         }
      } catch (FileNotFoundException e) {
         Logger.log(e.getMessage());
      }
   }
}
