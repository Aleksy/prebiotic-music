package aleksy.prebioticmusic.archean.geneticmelody.command;

import aleksy.prebioticframework.evolution.genetic.common.enumerate.GeneticSelectionMode;
import aleksy.prebioticmusic.archean.geneticmelody.constants.BasicConstants;
import aleksy.prebioticmusic.archean.geneticmelody.constants.FileConstants;
import aleksy.prebioticmusic.archean.geneticmelody.util.Configs;
import aleksy.prebioticmusic.archean.geneticmelody.util.Verify;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.annotation.Command;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.logic.Logger;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.model.AbstractCommand;
import javafx.scene.control.SelectionMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

@Command(name = "config", description = "manages the configurations of genetic algorithm")
public class ConfigCommand extends AbstractCommand {

   @Override
   protected String displayHelp() {
      return "manages the configurations of genetic algorithm. Calling with no arguments shows actual configurations" +
         " settings." +
         "\n\nAvailable arguments:" +
         "\ng - amount of generations" +
         "\np - amount of units in population" +
         "\nsm - selection mode (RANKING, ROULETTE, THRESHOLD_REPRODUCTION, TOURNAMENT)" +
         "\nm - mutation chance" +
         "\n\nAvailable flags:" +
         "\ndef - restores default configurations" +
         "\n\nExamples: " + BasicConstants.COMMAND + " " + getCommand() + " :g 100 :p 1000 :sm RANKING :m 0.02" +
         "\n          " + BasicConstants.COMMAND + " " + getCommand() + " :g 2000" +
         "\n          " + BasicConstants.COMMAND + " " + getCommand() + " !def";
   }

   @Override
   protected void run() {
      if (Verify.initialization(getCallDirectory())) {
         try {
            String configs = Configs.readConfigFile(getCallDirectory());
            if (!getFlags().contains("!def"))
               configs = changeConfigs(configs);
            else {
               Logger.log("Restoring the default configurations");
               configs = FileConstants.CONFIG_FILE_DEFAULT_CONTENT;
            }
            FileOutputStream fileOutputStream = new FileOutputStream(new File(getCallDirectory() + "//." +
               BasicConstants.COMMAND + "\\" + FileConstants.CONFIG_FILE_NAME));
            fileOutputStream.write(configs.getBytes());
            Logger.log("\n" + configs);
         } catch (IOException e) {
            Logger.log(e.getMessage());
         }

      } else {
         Logger.log(BasicConstants.COMMAND + " is not initialized here.");
      }
   }

   private String changeConfigs(String configs) {
      if (validateArguments()) {
         String generationsString = configs.substring(configs.indexOf("generations=") + "generations=".length(),
            configs.indexOf("population") - 1);
         String populationString = configs.substring(configs.indexOf("population=") + "population=".length(),
            configs.indexOf("selection_mode") - 1);
         String selectionModeString = configs.substring(configs.indexOf("selection_mode=") + "selection_mode=".length(),
            configs.indexOf("mutation_chance") - 1);
         String mutationChanceString = configs.substring(configs.indexOf("mutation_chance=") + "mutation_chance=".length(),
            configs.length() - 1);
         generationsString = checkChange("g", generationsString, "generations");
         populationString = checkChange("p", populationString, "population");
         selectionModeString = checkChange("sm", selectionModeString, "selection mode");
         mutationChanceString = checkChange("m", mutationChanceString, "mutation chance");
         return "generations=" + generationsString +
            "\r\npopulation=" + populationString +
            "\r\nselection_mode=" + selectionModeString +
            "\r\nmutation_chance=" + mutationChanceString + "\n";
      }
      return configs;
   }

   private boolean validateArguments() {
      int invalid = 0;
      String generations = getArguments().get("g");
      String population = getArguments().get("p");
      String selectionMode = getArguments().get("sm");
      String mutationChance = getArguments().get("m");
      if (generations != null) {
         if (!validatePositiveNumber(generations)) {
            invalid++;
            Logger.log("Argument 'g' is invalid.");
         }
      }
      if (population != null) {
         if (!validatePositiveNumber(population)) {
            invalid++;
            Logger.log("Argument 'p' is invalid.");
         }
      }
      if (mutationChance != null) {
         if (!validateRealNumber(mutationChance)) {
            invalid++;
            Logger.log("Argument 'm' is invalid.");
         }
      }
      if (selectionMode != null) {
         if (!validateSelectionMode(selectionMode)) {
            invalid++;
            Logger.log("Argument 'sm' is invalid.");
         }
      }
      return invalid == 0;
   }

   private boolean validateSelectionMode(String selectionMode) {
      try {
         GeneticSelectionMode.valueOf(selectionMode);
      } catch (IllegalArgumentException e) {
         return false;
      }
      return true;
   }

   private boolean validateRealNumber(String string) {
      double d;
      try {
         d = Double.parseDouble(string);
      } catch (NumberFormatException e) {
         return false;
      }
      return d > 0.0 && d < 1.0;
   }

   private boolean validatePositiveNumber(String string) {
      int integer;
      try {
         integer = Integer.parseInt(string);
      } catch (NumberFormatException e) {
         return false;
      }
      return integer > 0;
   }

   private String checkChange(String argument, String old, String listenedValue) {
      String argValue = getArguments().get(argument);
      if (argValue != null) {
         Logger.log(listenedValue + " value has been changed from " + old + " to " + argValue);
         return argValue;
      } else return old;
   }
}
