package aleksy.prebioticmusic.archean.geneticmelody.command;

import aleksy.prebioticframework.common.api.PrebioticApi;
import aleksy.prebioticframework.common.impl.Prebiotic;
import aleksy.prebioticframework.evolution.genetic.common.enumerate.GeneticSelectionMode;
import aleksy.prebioticframework.evolution.genetic.common.exception.GeneticAlgorithmException;
import aleksy.prebioticframework.evolution.genetic.common.model.config.GeneticAlgorithmConfigurations;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.Geneticable;
import aleksy.prebioticmusic.archean.geneticmelody.constants.BasicConstants;
import aleksy.prebioticmusic.archean.geneticmelody.logic.GeneticPreparation;
import aleksy.prebioticmusic.archean.geneticmelody.model.GeneticPhrase;
import aleksy.prebioticmusic.archean.geneticmelody.model.MusicEnvironment;
import aleksy.prebioticmusic.archean.geneticmelody.util.Configs;
import aleksy.prebioticmusic.archean.geneticmelody.util.Verify;
import aleksy.prebioticmusic.archean.musicmodel.builder.MusicBuilder;
import aleksy.prebioticmusic.archean.musicmodel.enumerate.PitchEnum;
import aleksy.prebioticmusic.archean.musicmodel.io.ArcheanMusicIO;
import aleksy.prebioticmusic.archean.musicmodel.model.Music;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.annotation.Command;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.logic.Logger;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.model.AbstractCommand;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

@Command(name = "run", description = "runs the genetic music creation")
public class RunCommand extends AbstractCommand {
   @Override
   protected String displayHelp() {
      return "runs the genetic music creation. Parameters:" +
         "\nf - filename to save the result" +
         "\nExample: " + BasicConstants.COMMAND + " " + getCommand() + " :f file";
   }

   @Override
   protected void run() {
      if (Verify.initialization(getCallDirectory())) {
         PrebioticApi api = Prebiotic.newInstance();
         api.enableEventLogging();
         String configs = null;
         try {
            configs = Configs.readConfigFile(getCallDirectory());
         } catch (FileNotFoundException e) {
            Logger.log(e.getMessage());
         }
         Map<String, String> map = Configs.generateMapFromConfigs(configs);
         int population = Integer.parseInt(map.get("population"));
         GeneticAlgorithmConfigurations config = createConfigs(api, map);

         List<Geneticable> firstPopulation = new GeneticPreparation()
            .prepare(population, 4, 4, 20, api);

         List<Geneticable> lastPopulation = null;

         try {
            lastPopulation = api.evolutionApi().geneticAlgorithm(firstPopulation,
               new MusicEnvironment(4, 4, 20), config);
         } catch (GeneticAlgorithmException e) {
            Logger.log(e.getMessage());
         }

         String filename = getArguments().get("f") == null ? "def" : getArguments().get("f");
         try {
            ArcheanMusicIO.write(((GeneticPhrase)(lastPopulation.get(0))).getFenotype(),
               getCallDirectory() + "\\" + filename);
         } catch (IOException e) {
            Logger.log(e.getMessage());
         }
         Logger.log("The melody has been saved to " + filename);
      } else {
         Logger.log(BasicConstants.COMMAND + " is not initialized here.");
      }
   }

   private GeneticAlgorithmConfigurations createConfigs(PrebioticApi api, Map<String, String> mappedConfigs) {
      int generations = Integer.parseInt(mappedConfigs.get("generations"));
      GeneticSelectionMode selectionMode = GeneticSelectionMode.valueOf(mappedConfigs.get("selection_mode"));
      double mutationChance = Double.parseDouble(mappedConfigs.get("mutation_chance"));
      return api.evolutionApi().getGeneticAlgorithmConfigurationsBuilder()
         .setSelectionMode(selectionMode)
         .setMutationChance(mutationChance)
         .setIterationsDisplaying(generations / 100)
         .setAmountOfGenerations(generations)
         .create();
   }
}
