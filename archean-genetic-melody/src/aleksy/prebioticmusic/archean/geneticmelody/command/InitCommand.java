package aleksy.prebioticmusic.archean.geneticmelody.command;

import aleksy.prebioticmusic.archean.geneticmelody.constants.BasicConstants;
import aleksy.prebioticmusic.archean.geneticmelody.constants.FileConstants;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.annotation.Command;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.logic.Logger;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.model.AbstractCommand;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

@Command(name = "init", description = "initializes the directory for the " + BasicConstants.NAME + " realization")
public class InitCommand extends AbstractCommand {

   @Override
   protected String displayHelp() {
      return "initializes the directory for the " + BasicConstants.NAME + " realization if the directory is not" +
         " initialized by the another realization.";
   }

   @Override
   protected void run() {
      if (checkDirectory()) {
         File dir = new File(getCallDirectory() + "\\." + BasicConstants.COMMAND);
         dir.mkdirs();
         File config = new File(getCallDirectory() + "\\." + BasicConstants.COMMAND + "\\"
            + FileConstants.CONFIG_FILE_NAME);
         try {
            config.createNewFile();
            FileOutputStream fileWriter = new FileOutputStream(config);
            fileWriter.write(FileConstants.CONFIG_FILE_DEFAULT_CONTENT.getBytes());
            fileWriter.close();
         } catch (IOException e) {
            Logger.log(e.getMessage());
         }
         Logger.log("Directory has been initialized to " + BasicConstants.NAME + " realization.");
         return;
      }
      Logger.log("Directory cannot be initialized. Some realization was initialized here.");
   }

   private boolean checkDirectory() {
      File pm = new File(getCallDirectory() + "\\pm");
      try {
         if (pm.createNewFile()) {
            FileWriter fileWriter = new FileWriter(pm);
            fileWriter.write("pm:" + BasicConstants.COMMAND);
            fileWriter.close();
            return true;
         }
      } catch (IOException e) {
         Logger.log(e.getMessage());
      }
      return false;
   }
}
