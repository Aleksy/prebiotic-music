package aleksy.prebioticmusic.archean.geneticmelody.logic;

import aleksy.prebioticframework.common.api.PrebioticApi;
import aleksy.prebioticframework.common.model.Range;
import aleksy.prebioticframework.evolution.genetic.builder.genotype.ChromosomeBuilder;
import aleksy.prebioticframework.evolution.genetic.builder.genotype.GenotypeBuilder;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.Geneticable;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.prebioticmusic.archean.geneticmelody.model.GeneticPhrase;
import aleksy.prebioticmusic.archean.musicmodel.enumerate.PitchEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 */
public class GeneticPreparation {
   public List<Geneticable> prepare(int population, int measuresPerBar, int cellsPerMeasure, int amountOfNotes, PrebioticApi api) {
      List<Geneticable> geneticables = new ArrayList<>();
      for(int i = 0; i < population; i++) {
         GeneticPhrase geneticPhrase = new GeneticPhrase(createGenotype(measuresPerBar, cellsPerMeasure, amountOfNotes, api));
         geneticPhrase.setAmountOfNotes(amountOfNotes);
         geneticPhrase.setCellsPerMeasure(cellsPerMeasure);
         geneticPhrase.setMeasuresPerBar(measuresPerBar);
         geneticables.add(geneticPhrase);
      }
      return geneticables;
   }

   private Genotype createGenotype(int measuresPerBar, int cellsPerMeasure, int amountOfNotes, PrebioticApi api) {
      ChromosomeBuilder chromosomeBuilder = api.evolutionApi().getGenotypeBuilder().addChromosome("phrase");
      Random random = new Random();
      for(int i = 0; i < amountOfNotes; i++) {
         chromosomeBuilder.addGene(132 * random.nextDouble(),
            new Range(0, 132), "note" + i + "_pitch");
         chromosomeBuilder.addGene(30 * random.nextDouble(),
            new Range(0, 30), "note" + i + "_duration");
         chromosomeBuilder.addGene(measuresPerBar * cellsPerMeasure * 20 * random.nextDouble(),
            new Range(0, 132), "note" + i + "_start_point");
      }
      return chromosomeBuilder.end().create();
   }
}
