package aleksy.prebioticmusic.archean.geneticmelody;

import aleksy.prebioticmusic.archean.geneticmelody.command.ConfigCommand;
import aleksy.prebioticmusic.archean.geneticmelody.command.InitCommand;
import aleksy.prebioticmusic.archean.geneticmelody.command.OpenCommand;
import aleksy.prebioticmusic.archean.geneticmelody.command.RunCommand;
import aleksy.prebioticmusic.archean.geneticmelody.constants.BasicConstants;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.exception.PcaeException;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.runner.PcaeRunner;

public class ArcheanGeneticMelodyApp {

   public static void main(String[] args) {
      PcaeRunner runner = new PcaeRunner();
      runner.setRealizationCommandName(BasicConstants.COMMAND);
      runner.setRealizationAuthor(BasicConstants.AUTHOR);
      runner.setRealizationVersion(BasicConstants.VERSION);
      runner.setRealizationName(BasicConstants.NAME);
      runner.addCommand(InitCommand.class);
      runner.addCommand(RunCommand.class);
      runner.addCommand(OpenCommand.class);
      runner.addCommand(ConfigCommand.class);
      try {
         runner.run(args);
      } catch (InstantiationException | IllegalAccessException | PcaeException e) {
         System.err.println(e.getMessage());
      }
   }
}
