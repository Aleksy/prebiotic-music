package aleksy.prebioticmusic.archean.geneticmelody.model;

import aleksy.prebioticframework.evolution.genetic.common.model.individual.Geneticable;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.Individual;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.genotype.Genotype;
import aleksy.prebioticmusic.archean.musicmodel.builder.MusicBuilder;
import aleksy.prebioticmusic.archean.musicmodel.logic.NoteLogic;
import aleksy.prebioticmusic.archean.musicmodel.model.Music;
import aleksy.prebioticmusic.archean.musicmodel.model.Note;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class GeneticPhrase extends Individual implements Geneticable {
   private int measuresPerBar;
   private int cellsPerMeasure;
   private int amountOfNotes;

   public GeneticPhrase(Genotype genotype) {
      super(genotype);
   }

   public void setMeasuresPerBar(int measuresPerBar) {
      this.measuresPerBar = measuresPerBar;
   }

   public void setCellsPerMeasure(int cellsPerMeasure) {
      this.cellsPerMeasure = cellsPerMeasure;
   }

   public void setAmountOfNotes(int amountOfNotes) {
      this.amountOfNotes = amountOfNotes;
   }

   public Music getFenotype() {
      MusicBuilder musicBuilder = new MusicBuilder();
      for (int i = 0; i < amountOfNotes; i++) {
         int pitch = genotype.findGeneValue("phrase:note" + i + "_pitch").intValue();
         int duration = genotype.findGeneValue("phrase:note" + i + "_duration").intValue();
         int startPoint = genotype.findGeneValue("phrase:note" + i + "_start_point").intValue();
         musicBuilder.addNote(
            NoteLogic.findPitchByNumber(pitch),
            startPoint, duration);
      }
      return musicBuilder.create(measuresPerBar, cellsPerMeasure);
   }

   @Override
   public Geneticable newInstance(Genotype genotype) {
      return new GeneticPhrase(genotype);
   }
}
