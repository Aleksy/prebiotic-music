package aleksy.prebioticmusic.archean.geneticmelody.model;

import aleksy.prebioticframework.evolution.genetic.common.model.environment.Environment;
import aleksy.prebioticframework.evolution.genetic.common.model.individual.Geneticable;
import aleksy.prebioticmusic.archean.musicmodel.model.Music;
import aleksy.prebioticmusic.archean.musicmodel.model.Note;

import java.util.List;

/**
 *
 */
public class MusicEnvironment extends Environment {
   private int measuresPerBar;
   private int cellsPerMeasure;
   private int amountOfNotes;

   public MusicEnvironment(int measuresPerBar, int cellsPerMeasure, int amountOfNotes) {
      this.measuresPerBar = measuresPerBar;
      this.cellsPerMeasure = cellsPerMeasure;
      this.amountOfNotes = amountOfNotes;
   }

   @Override
   public void rate(Geneticable geneticable) {
      double fitness = 0.0;
      GeneticPhrase geneticPhrase = (GeneticPhrase) geneticable;
      geneticPhrase.setAmountOfNotes(amountOfNotes);
      geneticPhrase.setCellsPerMeasure(cellsPerMeasure);
      geneticPhrase.setMeasuresPerBar(measuresPerBar);
      Music music = geneticPhrase.getFenotype();
      List<Note> notes = music.getNotes();
      if(music.calculateAmbitus() > 14)
         fitness -= 0.5;
      if(music.getScaleIdentifier().getSimilarity() < 2.8)
         fitness += 1 / music.getScaleIdentifier().getSimilarity();
      geneticPhrase.setFitness(fitness);
   }
}
