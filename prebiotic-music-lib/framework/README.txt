Prebiotic Framework 1.0.2
Neural networks, deep learning and evolution algorithms experimental library.

Author: Aleksy Bernat, Wroclaw

For more informations about license and instructions see:
https://gitlab.com/Aleksy/prebiotic-framework
