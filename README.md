# Prebiotic Music

The PM is a group of the project connecting the artificial intelligence with the music.
Each part of the PM is the research project. Within each project I will implement
the so-called "realizations". Each realization is described below.

## Basic realizations

Non-research realizations of this big project are: PM - that's the simple program,
which basically can check if the directory is already initialized by any PM realization,
and the second one is the Prebiotic Command Application Engine - that's a simple framework,
I'm using for creating "console realizations".

## Archean

I'm actually working on this project. Archean is the first research project from
PM. There are two or three realizations that I want to implement: music model,
genetic algorithm and neural network. The Archean is the first try to generate some
music by the AI. I want to create genetic algorithm melody generator, and maybe
the neural network melody generator. More informations about realizations can be
find in the wiki.