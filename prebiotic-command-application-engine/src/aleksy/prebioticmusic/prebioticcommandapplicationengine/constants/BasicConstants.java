package aleksy.prebioticmusic.prebioticcommandapplicationengine.constants;

/**
 * Basic constants values
 */
public class BasicConstants {
   /**
    * Name
    */
   public static final String NAME = "Prebiotic Command Application Engine";
   /**
    * Version
    */
   public static final String VERSION = "2.0.0";
   /**
    * Author
    */
   public static final String AUTHOR = "Aleksy Bernat, Wroclaw";
}
