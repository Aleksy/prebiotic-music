package aleksy.prebioticmusic.prebioticcommandapplicationengine.logic;

/**
 * Simple message logger
 */
public class Logger {
   /**
    * Logs a message
    * @param message to log
    */
   public static void log(String message) {
      System.out.println(message);
   }
}
