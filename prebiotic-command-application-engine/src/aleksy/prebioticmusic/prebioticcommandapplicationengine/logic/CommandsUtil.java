package aleksy.prebioticmusic.prebioticcommandapplicationengine.logic;

import aleksy.prebioticmusic.prebioticcommandapplicationengine.annotation.Command;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.exception.PcaeException;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.model.AbstractCommand;

import java.util.List;

/**
 * Commands util class
 */
public class CommandsUtil {
   /**
    * Splits the arguments to the new command instance
    * @param args of the program
    * @param commands list
    * @return new instance of command or null when is not recognized
    * @throws IllegalAccessException when newInstance() method of the command class throws an exception
    * @throws InstantiationException when newInstance() method of the command class throws an exception
    * @throws PcaeException when command syntax is wrong
    */
   public AbstractCommand split(String[] args, List<Class<? extends AbstractCommand>> commands)
      throws IllegalAccessException, InstantiationException, PcaeException {
      AbstractCommand commandInstance = null;
      for (Class<? extends AbstractCommand> command : commands) {
         String value = command.getDeclaredAnnotation(Command.class).name();
         if (value.equals(args[1])) {
            commandInstance = command.newInstance();
            commandInstance.setCommand(args[1]);
         }
      }
      boolean subcommandExists = false;
      if (commandInstance != null) {
         for (int i = 1; i < args.length; i++) {
            String arg = args[i];
            if (arg.startsWith("::") && subcommandExists) {
               throw new PcaeException("Only one subcommand can be added.");
            }
            if (arg.startsWith("::") && !subcommandExists) {
               commandInstance.setSubcommand(arg);
               subcommandExists = true;
            }
            if (arg.startsWith(":") && arg.charAt(1) != ':') {
               if (i == args.length - 1)
                  throw new PcaeException("No value for parameter '" + arg.substring(1) + "'");
               if (args[i + 1].startsWith(":") || args[i + 1].startsWith("!"))
                  throw new PcaeException("No value for parameter '" + arg.substring(1) + "'");
               commandInstance.getArguments().put(arg.substring(1), args[i + 1]);
            }
            if (arg.startsWith("!")) {
               commandInstance.getFlags().add(arg.substring(1));
            }
         }
         commandInstance.setCallDirectory(args[0]);
      }
      return commandInstance;
   }
}
