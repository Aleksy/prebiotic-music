package aleksy.prebioticmusic.prebioticcommandapplicationengine;

import aleksy.prebioticmusic.prebioticcommandapplicationengine.exception.PcaeException;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.logic.Logger;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.runner.PcaeRunner;

/**
 * Prebiotic AbstractCommand Application Engine App
 */
public class PrebioticCommandApplicationEngineApp {

    /**
     * Main method
     * @param args of program
     */
    public static void main(String[] args) {
        try {
            new PcaeRunner().run(args);
        } catch (InstantiationException | IllegalAccessException | PcaeException e) {
            Logger.log(e.getMessage());
        }
    }
}
