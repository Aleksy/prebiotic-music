package aleksy.prebioticmusic.prebioticcommandapplicationengine.model;

import aleksy.prebioticmusic.prebioticcommandapplicationengine.logic.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Abstract command class
 */
public abstract class AbstractCommand {
   private String command;
   private String subcommand;
   private Map<String, String> arguments;
   private List<String> flags;
   private String callDirectory;

   /**
    * Constructor
    */
   public AbstractCommand() {
      arguments = new HashMap<>();
      flags = new ArrayList<>();
   }

   /**
    * Getter for the arguments
    * @return map of arguments
    */
   public Map<String, String> getArguments() {
      return arguments;
   }

   /**
    * Setter for the command
    * @param command to set
    */
   public void setCommand(String command) {
      this.command = command;
   }

   /**
    * Getter for the command
    * @return command
    */
   public String getCommand() {
      return command;
   }

   /**
    * Setter for the subcommand
    * @param subcommand to set
    */
   public void setSubcommand(String subcommand) {
      this.subcommand = subcommand;
   }

   /**
    * Getter for the subcommand
    * @return subcommand
    */
   public String getSubcommand() {
      return subcommand;
   }

   /**
    * Getter for the flags
    * @return flags
    */
   public List<String> getFlags() {
      return flags;
   }

   /**
    * Setter for call directory
    * @param callDirectory to set
    */
   public void setCallDirectory(String callDirectory) {
      this.callDirectory = callDirectory;
   }

   /**
    * Getter for call directory
    * @return call directory
    */
   public String getCallDirectory() {
      return callDirectory;
   }

   /**
    * Calls the command. If command contains the "!h" flag then
    * method displays the help description of the command, if not
    * - the run() method is called.
    */
   public void call() {
      if(flags.contains("h")) {
         Logger.log(displayHelp());
         return;
      }
      run();
   }

   /**
    * Returns the help description
    */
   protected abstract String displayHelp();

   /**
    * Run method for the command
    */
   protected abstract void run();
}
