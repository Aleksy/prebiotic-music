package aleksy.prebioticmusic.prebioticcommandapplicationengine.runner;

import aleksy.prebioticmusic.prebioticcommandapplicationengine.annotation.Command;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.constants.BasicConstants;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.exception.PcaeException;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.logic.CommandsUtil;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.logic.Logger;
import aleksy.prebioticmusic.prebioticcommandapplicationengine.model.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

/**
 * Engine runner. To run the realization application create the new instance of this runner
 * and call two methods: "setRealizationName()" to set information about version, name and author,
 * and "addCommand" to add own command of the realization.
 *
 */
public class PcaeRunner {
   private List<Class<? extends AbstractCommand>> commands;
   private String realizationName;
   private String realizationCommandName;
   private String realizationVersion;
   private String realizationAuthor;

   /**
    * Constructor
    */
   public PcaeRunner() {
      commands = new ArrayList<>();
      realizationName = BasicConstants.NAME;
   }

   /**
    * Run method of the runner class. This method process the command from the args.
    * @param args of the program
    * @throws InstantiationException for problem with instantiate the command class
    * @throws IllegalAccessException for problem with instantiate the command class
    * @throws PcaeException when command is wrong
    */
   public void run(String[] args) throws InstantiationException, IllegalAccessException, PcaeException {
      if(args.length == 1) {
         Logger.log("Empty command. To see available commands type '" + realizationCommandName + " help'");
         return;
      }

      if (args[1].equals("help")) {
         displayHelp();
         return;
      }

      if (args[1].equals("info")) {
         displayInfo();
         return;
      }

      AbstractCommand command = new CommandsUtil().split(args, commands);
      if (command == null) {
         Logger.log(args[1] + " is not a recognized command");
         return;
      }
      command.call();
   }

   /**
    * Adds a new command to the runner.
    * @param commandClass to add
    */
   public void addCommand(Class<? extends AbstractCommand> commandClass) {
      commands.add(commandClass);
   }

   /**
    * Setter for realization name
    * @param realizationName to set
    */
   public void setRealizationName(String realizationName) {
      this.realizationName = realizationName;
   }

   /**
    * Setter for realization author
    * @param realizationAuthor to set
    */
   public void setRealizationAuthor(String realizationAuthor) {
      this.realizationAuthor = realizationAuthor;
   }

   /**
    * Setter for realization command name. It is the name which is typed
    * to the console when user calls the program
    * @param realizationCommandName to set
    */
   public void setRealizationCommandName(String realizationCommandName) {
      this.realizationCommandName = realizationCommandName;
   }

   /**
    * Setter for realization version
    * @param realizationVersion to set
    */
   public void setRealizationVersion(String realizationVersion) {
      this.realizationVersion = realizationVersion;
   }

   private void displayHelp() {
      for (Class<? extends AbstractCommand> command : commands) {
         Logger.log(command.getDeclaredAnnotation(Command.class).name() + " - "
            + command.getDeclaredAnnotation(Command.class).description());
      }
      Logger.log("info - displays the information about the realization");
      Logger.log("help - displays names and descriptions of the commands\n");
      Logger.log("To call the command with the arguments type:\n"
         + realizationCommandName + " command_name :arg0 val0 :arg1 val1" +
         "\nTo add the flag to the command type:" +
         "\n" + realizationCommandName + " command_name !flag0 !flag1\n");
   }

   private void displayInfo() {
      Logger.log(realizationName + " " + realizationVersion + "\n" + realizationAuthor);
      Logger.log("\nApplication runs on " + BasicConstants.NAME + " " + BasicConstants.VERSION);
   }
}
