package aleksy.prebioticmusic.prebioticcommandapplicationengine.exception;

/**
 * PCAE main exception
 */
public class PcaeException extends Exception {
   /**
    * Constructor
    * @param message of the exception
    */
   public PcaeException(String message) {
      super("PCAE Exception: " + message);
   }
}
